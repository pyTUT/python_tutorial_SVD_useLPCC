# sklearnBayes.py
* acc on test: 0.650863418511
* acc on valid: 0.57954105191

# sklearnDecisionTree.py
* acc on test: 0.605085378483
* acc on valid: 0.568095930694

# sklearnMLP.py
* acc on test: 0.687627947304
* acc on valid: 0.586317935559

# sklearnNearest.py
* acc on test: 0.679710267094
* acc on valid: 0.567905390385

# sklearnSGD.py
* acc on test: 0.681853760748
* acc on valid: 0.593380629672